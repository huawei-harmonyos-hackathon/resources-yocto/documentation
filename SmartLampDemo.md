# Smart Lamp Demo
A simple smart lamp demo application and Home Assistant integration    

## Description
This demo is using Yocto Linux as base operating system and does the simple task of switching on and off one LED using a GPIO pin.    
The demo makes use of two components, Smart Lamp Demo application and the MQTT Manager that interact via DBus on target IoT device.    

## Dependencies
Please make sure the hardware and software dependencies are available    

### Hardware
* One Raspberry Pi4 board including accesories - https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
* USB to UART adaptor connected to UART pins on RPi4 (for debug)
* One LED with wires connected on GPIO21

### Software
* Access to the Hackthon resources on gitlab.com - Should be done during hackathon onboarding
* Yocto Linux build environment

## Replicate Smart Lamp Demo
To replicate the Smart Lamp Demo we need to build Poky with meta-raspberrypi, meta-openembedded and meta-dpe-high (provides SmartLamp Demo and MQTT-Manager).    

### Device
Build and configure the Linux image for RPi4:
1. Setup Yocto Linux workspace - See https://gitlab.com/harmonyos-hackathon/resources-yocto/documentation/-/blob/main/GettingStarted.md
2. Install bmaptool in your host machine
```
# sudo apt-get install bmap-tools
```
3. Add meta-raspberrypi, meta-openembedded and meta-dpe-high to conf/bblayers.conf
4. Add connman, smartlamp-demo and mqtt-manager to IMAGE_INSTALL list in your conf/local.conf file
```
IMAGE_INSTALL_append = " mqtt-manager smartlamp-demo connman connman-client iptables"
```
5. Add systemd, wifi and bluetooth as distro features in your conf/local.conf file

```
DISTRO_FEATURES_append = " systemd bluetooth wifi"
VIRTUAL-RUNTIME_init_manager = "systemd"
```
6. Enable UART for debugging in your conf/local.conf file

```
ENABLE_UART = "1"
```
7. Build the image. To have graphics you can use core-image-weston or can use a smaller with less dependency target image supported in Yocto or meta-raspberrypi
```
# MACHINE=raspberrypi4-64 bitbake core-image-weston
```
8. Flash the image to SD card
```
# sudo bmaptool copy core-image-weston-raspberrypi4-64.wic.bz2 /dev/<sd_card_node>
```
9. Unmount SD card from host, insert into RaspberryPi4 device and boot
10. Connect the device on you local network
The image comes with connman service installed and can be used to bring the device online using WiFi or ethernet    
A good starting point for connman is Arch wiki: https://wiki.archlinux.org/title/ConnMan    
```
$ connmanctl
connmanctl> enable wifi
connmanctl> scan wifi
connmanctl> services
connmanctl> agent on
connmanctl> connect <connection>
```
    
### MQTT broker
To connect the device with Home Assistant the MQTT broker server should be availble.    
Any public MQTT broker can be used or participants can setup a mosquitto broker on local network.     
Make sure that the device is using this broker address in /etc/mqttmanager.conf    

### Home Assistant 
An instance of home assistant needs to be setup on the local machine or in the cloud.    
The home assistant provides MQTT integration: https://www.home-assistant.io/docs/mqtt/discovery/    
Once the Home Assistant is running the device can be added as an MQTT switch device.    
Please note that the topic used by mqtt-manager is "/huawei/smartlamp/switch".    
For more information please check the mqm-adapter module:   
https://gitlab.com/harmonyos-hackathon/resources-yocto/mqtt-manager/-/blob/main/source/mqm-adapter.c
