# Getting Started
How to use HarmonyOS Hackathon resources

## Resources
The goal of the hackathon is to create innovative home automation scenarios with devices running Yocto Linux.    
To support participants we provide several demo projects and recipes to integrate the projects into Yocto Linux device images.     
We can group the resources by demo projects and integration meta-layer.   

### Meta layer
1. Meta DPE High - The Yocto meta layer that provides recipes targeting Yocto Linux with recipes for demo projects 
https://gitlab.com/harmonyos-hackathon/resources-yocto/meta-dpe-high

### Demo projects
1. Smart Lamp Demo - A simple demo application that registers for lamp state events on DBus and set the GPIO controilling the lamp switch
https://gitlab.com/harmonyos-hackathon/resources-yocto/smart-lamp-demo
2. MQTT Manager - Example of MQTT manager that register, publish and subscribe to a broker a topic defining a smart lamp switch
https://gitlab.com/harmonyos-hackathon/resources-yocto/mqtt-manager  

## Building target image
Please follow the steps bellow to prepare the workspace and build the sample target image:       
1. Get access to Harmony OS Hackathon resources group and resources projects
* Request access to resources if not available for your GitLab account
* Generate an access token in GitLab to use in your build machine
* Add gitlab.com machine with the access token in your ~/.netrc file
```
machine gitlab.com
  login <username>
  password <access_token>
```

2. Setup build environment for Yocto (Dunfell LTS):    
https://docs.yoctoproject.org/3.1.11/

3. Setup the build workspace    
```
# mkdir ohos && cd ohos
# git clone git://git.yoctoproject.org/poky -b dunfell
# git clone git://git.yoctoproject.org/meta-raspberrypi -b dunfell
# git clone git://git.openembedded.org/meta-openembedded -b dunfell
# git clone git@gitlab.com:harmonyos-hackathon/resources-yocto/meta-dpe-high.git
```

4. Use meta-dpe-high meta layer instructions to integrate and build with this meta layer
https://gitlab.com/harmonyos-hackathon/resources-yocto/meta-dpe-high/-/blob/main/README.md

5. Follow demo specific instructions 
https://gitlab.com/harmonyos-hackathon/resources-yocto/documentation/-/blob/main/SmartLampDemo.md   

## Automation environment
To demonstrate the automation capabilities of smart home scenarios the participants will create, we propose to open source automation platforms and services.   

### Platforms
Any open source automation platform can be used, this includes (not limiting) one of:   
* Home Assistant - https://www.home-assistant.io/
* OpenHAB - https://www.openhab.org/
* Node-RED - https://nodered.org/

### Services
Additional open source servers can be used as long as the configuration is shared as part of submissions.    
The Smart Lamp Demo provided with Hackathon resources is using MQTT as an example for IPC with the automation platform.    
* Mosquitto - https://mosquitto.org 

