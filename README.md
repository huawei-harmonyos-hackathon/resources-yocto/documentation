# Welcome to HarmonyOS Hackathon
Let's play with HarmonyOS and craft the next gen IoT devices and user scenarios for smart home automation

## Content
1. Getting Started - https://gitlab.com/harmonyos-hackathon/resources-yocto/documentation/-/blob/main/GettingStarted.md
1. Smart Lamp Demo - https://gitlab.com/harmonyos-hackathon/resources-yocto/documentation/-/blob/main/SmartLampDemo.md
